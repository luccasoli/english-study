import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const STORAGE_KEY = 'english-study';

@Injectable()
export class StudyFileProvider {

  constructor(public storage: Storage) {
  }

  insert(englishFile: EnglishFile) {
    this.storage.get(STORAGE_KEY)
      .then(sucess => {
        if(!sucess) {
          console.log("A lista não existe, criando uma vazia")
          let lista: EnglishFile[] = [];
          lista.push(englishFile);

          this.storage.set(STORAGE_KEY, JSON.stringify(lista));
        } else {
          sucess = JSON.parse(sucess);
          sucess.push(englishFile);

          this.storage.set(STORAGE_KEY, JSON.stringify(sucess));
        }
        console.log("insert concluiu")
      });
  }

  updateStorage(listFiles: any[]) {
    this.getAllEnglishStudyFiles()
      .then(sucess => {
        this.storage.set(STORAGE_KEY, JSON.stringify(listFiles));
      });
  }

  remove(englishFile: EnglishFile) { }

  getAllEnglishStudyFiles() {
    return this.storage.get(STORAGE_KEY);
  }

}

export class EnglishFile {

  constructor(
    englishPDFPath?,
    englishPDFName?, 
    portuguesePDFPath?, 
    portuguesePDFName?,
    audioPath?,
    audioName?) {

    this.englishPDFPath = englishPDFPath;
    this.englishPDFName = englishPDFName;

    this.portuguesePDFPath = portuguesePDFPath;
    this.portuguesePDFName = portuguesePDFName;

    this.audioPath = audioPath;
    this.audioName = audioName;
  }

  englishPDFPath: string = "sss";
  englishPDFName: string;

  portuguesePDFPath: string;
  portuguesePDFName: string;

  audioPath: string;
  audioName: string;

  timesPlayed: number = 0;
}

export class EnglishFileList {
  list: Array<EnglishFile>;
}