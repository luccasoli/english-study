import { NgModule } from '@angular/core';
import { IonicPageModule, ModalController } from 'ionic-angular';
import { FilesPage } from './files';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { MusicControls } from '@ionic-native/music-controls';
import { File } from '@ionic-native/file';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { FilePath } from '@ionic-native/file-path';
import { Media, MediaObject } from '@ionic-native/media';
import { AddFilePageModule } from '../add-file/add-file.module';
import { EditFilePageModule } from '../edit-file/edit-file.module';

@NgModule({
  declarations: [
    FilesPage,
  ],
  imports: [
    AddFilePageModule,
    EditFilePageModule,
    IonicPageModule.forChild(FilesPage),
  ],
  providers: [
    ModalController,
    FileChooser,
    FileOpener,
    MusicControls,
    File,
    DocumentViewer,
    FilePath,
    Media
  ],
  exports: [
    FilesPage
  ]
})
export class FilesPageModule {}
