import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { MusicControls } from '@ionic-native/music-controls';
import { File, Entry, FileEntry } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FilePath } from '@ionic-native/file-path';
import { Media, MediaObject } from '@ionic-native/media';
import { Storage } from '@ionic/storage';
import { PopoverController } from 'ionic-angular'
import { StudyFileProvider, EnglishFile } from '../../providers/study-file/study-file';
import { ViewerPage } from '../viewer/viewer';

@IonicPage()
@Component({
  selector: 'page-files',
  templateUrl: 'files.html',
})
export class FilesPage {

  listFiles: any[] = [];


  constructor(
    private app: App,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private fileChooser: FileChooser,
    private fileOpener: FileOpener,
    private musicControls: MusicControls,
    private document: DocumentViewer,
    private file: File,
    private filePath: FilePath,
    private media: Media,
    private storage: Storage,
    public popoverCtrl: PopoverController,
    public studyFileProvider: StudyFileProvider) { }

  ionViewDidLoad() {
    this.update();
  }

  execute(file: EnglishFile) {
    this.navCtrl.push('ViewerPage');
  }

  chooseFile() {

    this.fileChooser.open()
      .then(path => {

        this.filePath.resolveNativePath(path)
          .then(nativePath => {

            this.file.resolveLocalFilesystemUrl(nativePath)
              .then(entry => {
                
                let fileEntry = entry as FileEntry;
                fileEntry.file(file => {

                  this.fileOpener.open(nativePath, file.type)
                    .then(sucess => {
                      console.log('Arquivo aberto com sucesso');
                    })
                    .catch(error => {
                      console.log('Erro: ' + error);
                    });
                })
              })
          })
      })
  }

  openLocalPdf() {
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    }
  }

  addItem(item: EnglishFile) { // Adiciona um EnglishFile a lista
    this.listFiles.push(item);
    this.studyFileProvider.insert(item);
  }

  showAddFile() { // Exibe modal
    let addFileModal = this.modalCtrl.create('AddFilePage');
    addFileModal.present();

    addFileModal.onDidDismiss((data: EnglishFile) => {
      if(data.englishPDFName != null) {
        this.addItem(data);
        console.log("Dado adicionado com sucesso");
      } else {
        console.log("Nenhum dado será adicionado!");
      }
    });
  }

  showEditFile() {
    let editFileModal = this.modalCtrl.create('EditFilePage');
    editFileModal.present();
  }

  removeItem(englishPDFName: string) {
    for (var i = 0; i < this.listFiles.length; i++) {
      if (this.listFiles[i].englishPDFName == englishPDFName) {
        this.listFiles.splice(i, 1);
        console.log("Item removido com sucesso");
        break;
      }
    }

    this.studyFileProvider.updateStorage(this.listFiles);
  }

  update() { // Atualiza a lista a partir dos dados armazenados
    this.studyFileProvider.getAllEnglishStudyFiles()
      .then(sucess => {
        console.log(sucess);
        sucess = JSON.parse(sucess);
        if (!sucess) {
          console.log("A lista não existe, criando uma vazia")
          let lista: EnglishFile[] = [];

          sucess = lista;
        }

        this.listFiles = sucess;
        console.log("update concluiu")
      })
  }
}
