import { NgModule } from '@angular/core';
import { IonicPageModule, ModalController, ViewController } from 'ionic-angular';
import { EditFilePage } from './edit-file';

@NgModule({
  declarations: [
    EditFilePage,
  ],
  imports: [
    IonicPageModule.forChild(EditFilePage),
  ],
  providers: [
  	ModalController,
  	ViewController
  ]
})
export class EditFilePageModule {}
