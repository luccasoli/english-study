import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { EnglishFile } from '../../providers/study-file/study-file';

/**
 * Generated class for the EditFilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-file',
  templateUrl: 'edit-file.html',
})
export class EditFilePage {

  englishFile: EnglishFile;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
    this.englishFile = new EnglishFile();
    this.englishFile.englishPDFName = "d";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditFilePage');
    this.englishFile = new EnglishFile();
    this.englishFile.englishPDFName = "d";
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
