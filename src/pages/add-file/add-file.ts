import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { EnglishFile } from '../../providers/study-file/study-file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { FileEntry, File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-add-file',
  templateUrl: 'add-file.html',
})
export class AddFilePage {

  englishPDFPath: string;
  englishPDFName: string;

  portuguesePDFPath: string;
  portuguesePDFName: string;

  audioPath: string;
  audioName: string;

  timesPlayed: number = 0;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private fileChooser: FileChooser,
    private file: File,
    private filePath: FilePath,
    private zone: NgZone) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddFilePage');
  }

  chooseEnglishPDF() {
    this.fileChooser.open()
      .then(path => {

        this.filePath.resolveNativePath(path)
          .then(nativePath => {

            this.englishPDFPath = nativePath; // nativePath
            this.file.resolveLocalFilesystemUrl(nativePath)
              .then(entry => {

                var fileEntry = entry as FileEntry;
                fileEntry.file(success => {
                  this.zone.run(() => {
                    this.englishPDFName = success.name;
                    console.log('aathis.englishPDFName atualizado');
                  });

                });
              })
              .catch(error => {

              });

          })
          .catch(err => {
            console.log("erro 38")
            console.log(path)
          });

      })
      .catch(e => {
        console.log("erro 29")
        console.log(e)
      });
  }

  choosePortuguesePDF() {
    this.fileChooser.open()
      .then(uri => {
        var path = uri;
        this.filePath.resolveNativePath(path)
          .then(filePath => {
            this.portuguesePDFPath = filePath; // nativePath

            this.file.resolveLocalFilesystemUrl(filePath)
              .then(entry => {
                var fileEntry = entry as FileEntry;
                fileEntry.file(success => {
                  
                  this.zone.run(() => {
                    this.portuguesePDFName = success.name;
                    console.log('this.portuguesePDFName atualizado');
                  });
                });
              })
              .catch(error => {

              });

          })
          .catch(err => {
            console.log("erro 38")
            console.log(path)
          });

      })
      .catch(e => {
        console.log("erro 29")
        console.log(e)
      });
  }

  chooseAudio() {
    this.fileChooser.open()
      .then(uri => {
        var path = uri;
        this.filePath.resolveNativePath(path)
          .then(filePath => {
            this.audioPath = filePath; // nativePath

            this.file.resolveLocalFilesystemUrl(filePath)
              .then(entry => {
                var fileEntry = entry as FileEntry;
                fileEntry.file(success => {
                  this.zone.run(() => {
                    this.audioName = success.name;
                    console.log('this.audioName atualizado');
                  });
                });
              })
              .catch(error => {

              });

          })
          .catch(err => {
            console.log("erro 38")
            console.log(path)
          });

      })
      .catch(e => {
        console.log("erro 29")
        console.log(e)
      });
  }

  dismiss() {
    let englishFile = new EnglishFile(
      this.englishPDFPath,
      this.englishPDFName,
      this.portuguesePDFPath,
      this.portuguesePDFName,
      this.audioPath,
      this.audioName
    );
    this.viewCtrl.dismiss(englishFile);
  }

}
