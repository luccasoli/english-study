import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddFilePage } from './add-file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    AddFilePage,
  ],
  imports: [
    IonicPageModule.forChild(AddFilePage),
  ],
  providers: [
    FileChooser,
    FileOpener,
    File,
    FilePath
  ]
})
export class AddFilePageModule {}
